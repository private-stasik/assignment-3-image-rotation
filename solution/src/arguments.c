#include "arguments.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>


#define UNCORRECT_ANGLE 1
#define FULL_TURNOVER 360
#define UNIT_OF_ROTATION 90
#define MODE_READ "r"
#define MODE_WRITE "w"
#define CNT_ARGS 4


static const char *const STATUSES_INFO[] = {
        [STATUS_OK] = "",
        [STATUS_ERROR_WRONG_ARGS_COUNT] = "Count arguments is not 4. Expected format: ./image-transformer <source-image> <transformed-image> <angle>\n",
        [STATUS_ERROR_OPEN_SOURCE_FILE] = "Couldn't open source (file) image for reading\n",
        [STATUS_ERROR_OPEN_TRANSF_FILE] = "Couldn't open transformer (file) image for writing or create new\n",
        [STATUS_ERROR_ANGLE_IS_NOT_CORRECT] = "Not correct angle. List of correct angles: {-270, -180, -90, 0, 90, 180, 270}\n"
};

static bool is_integer(const char* str){
    if (!str) return false;
    int i = 0;
    i += *str == '+' || *str == '-'; 
    for (; i < strlen(str); ++i){
        if (!isdigit(str[i])){
            return false;
        }
    }
    return true;
}

static bool is_not_big_integer(const char *str){
    return strlen(str) <= 4 && is_integer(str);
}

static bool check_argc(int argc, struct arguments* args){
    if (argc != CNT_ARGS){
        args->status = STATUS_ERROR_WRONG_ARGS_COUNT;
        return false;
    }
    return true;
}

static bool check_angle(const char* angle_str, struct arguments* args){
    int16_t angle = UNCORRECT_ANGLE;

    if (is_not_big_integer(angle_str)){
        angle = (int16_t) atoi(angle_str);
        angle += FULL_TURNOVER;
    }

    if (angle >= 0 && angle % UNIT_OF_ROTATION == 0){
        args->rotation_cnt = (int8_t)((angle / UNIT_OF_ROTATION) % 4);
    } else {
        args->status = STATUS_ERROR_ANGLE_IS_NOT_CORRECT;
        return false;
    }

    return true;
}

static bool check_source_file(const char* name_source_file, struct arguments* args){
    FILE* file = fopen(name_source_file, MODE_READ);
    if (file){
        args->source_file = file;
        return true;
    }
    args->status = STATUS_ERROR_OPEN_SOURCE_FILE;
    return false;
}

static bool check_transf_file(const char* name_transf_file, struct arguments* args){
    FILE* file = fopen(name_transf_file, MODE_WRITE);
    if (file){
        args->transf_file = file;
        return true;
    }
    args->status = STATUS_ERROR_OPEN_TRANSF_FILE;
    return false;
}

struct arguments* convert_arguments(int argc, const char** argv){

    struct arguments* result = malloc(sizeof(struct arguments));

    if (!result){
        return 0;
    }

    result->status = STATUS_OK;

    if (!check_argc(argc, result)){
        return result;
    }

    bool (*checkers[CNT_ARGS - 1])(const char*, struct arguments*) = {check_source_file, check_transf_file, check_angle};
 
    for (int i = 0; i < CNT_ARGS - 1; ++i){
        if (!checkers[i](argv[i + 1], result)){
            return result;
        }
    }

    return result;

}

void destroy_arguments(struct arguments* args){
    if (args){
        if (args->source_file)
            fclose(args->source_file);
        if (args->transf_file)
            fclose(args->transf_file);
        free(args);
    }
}

void print_error(enum status_code status){
    if (status != STATUS_OK)
        fprintf(stderr, "%s", STATUSES_INFO[status]);
}
