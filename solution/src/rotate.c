#include "rotate.h"
#include <string.h>

struct image rotate( struct image const source ){

    struct image rot_img = create_image(source.height, source.width);

    if (!rot_img.data || !source.data){
        return rot_img;
    }

    for (int32_t i = 0; i < source.height; ++i)
        for (int32_t j = 0; j < source.width; ++j)
            rot_img.data[source.height * (source.width - j - 1) + i] = source.data[source.width * i + j];
    
    return rot_img;
}

struct image rotate_on( struct image const source, int8_t cnt){

    struct image rot_img = source;

    while (cnt--){
        destroy_image(&rot_img);
        rot_img = rotate(rot_img);
    }
    
    return rot_img;
}
