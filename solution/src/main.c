#include "../include/bmp.h"
#include "arguments.h"
#include "rotate.h"
#include <stdlib.h>

static void clearing_memory(struct image *img, struct arguments* args){
    destroy_image(img);
    destroy_arguments(args);
}

int main( int argc, const char** argv ) {
    struct arguments* args = convert_arguments(argc, argv);

    if (args == 0){
        fprintf(stderr, "Couldn't allocate memory for converted arguments");
        exit(EXIT_FAILURE);
    }
    if (args->status != STATUS_OK){
        print_error(args->status);
        destroy_arguments(args);
        exit(EXIT_FAILURE);
    }

    struct image img;
    int8_t rot_cnt = args->rotation_cnt;

    enum read_status rs = from_bmp(args->source_file, &img);

    if (rs != READ_OK){
        print_read_error(rs);
        clearing_memory(&img, args);
        exit(EXIT_FAILURE);
    }

    while (rot_cnt--){
        struct image rot_img = rotate(img);
        destroy_image(&img);
        img = rot_img;
    }

    enum write_status ws = to_bmp(args->transf_file, &img);

    if (ws != WRITE_OK){
        print_write_error(ws);
        clearing_memory(&img, args);
        exit(EXIT_FAILURE);
    }

    clearing_memory(&img, args);

    return 0;
}
